import React from 'react';
import { NavLink } from 'react-router-dom';

//asset
import logo from '../assets/asset_docpage/ic-logo.png';
import arrowrg from '../assets/asset_docpage/btn_arrow-right.png';
import roadmap from '../assets/asset_roadmap/roadmap.png';
//asset style

// import '../components/style/global.css';

//globalcomponent
import Navbar from '../components/comp/navbar.js';
import Footer from '../components/comp/footer.js'


const Docpage = () => {
    require ('../css/roadmap.css')
    return (
        <>
           <Navbar img={logo} color="#0E93F5"/>
            <div>

                  {/* <!-- sec-hero --> */}
            <div className="section roadmap-landing-sec1">
                <div className="container">
                  <div className="roadmap-main-box flex-c h-center">
                      <h1 className=" roadmap-display t-center">
                      Roadmap
                      </h1>
                  </div>
                </div>
              </div>

                  {/* <!-- sec-roadmap --> */}
                  <div className="section">
                      <div className="container">
                        {/* <!-- mein-content --> */}

                                <div className="sec-roadmap scrolloverflow">
                                    {/* slider is img cz its not complete content yet please call us when its complate */}
                                    <img src={roadmap}/>
                                </div>






                      </div>
                  </div>



            </div>
           <Footer color="#0E93F5"/>
        </>
    );
};

export default Docpage;

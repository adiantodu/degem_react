import $ from 'jquery'
import React, { useState } from "react";
import { NavLink, Link } from "react-router-dom";
import Footer from '../components/comp/footer.js'

import logo from '../assets/asset_refinery/logo-refinery.png'
import bg_refinery from '../assets/asset_refinery/bg-refinery.png'


function Refinery_paper(){
    require('../css/refinery_paper.css')

    const [mobileShow, setMobileShow] = useState(false)
    const handleMobileShow = () => {
        setMobileShow(prev => !prev)
    }
    return(
        <>
        <div className="refinery-paper">
        <header>
            <div className="container-refinery-paper">
            <nav className="navbar-refinery-paper">
                <a href="#" className="nav-logo-refinery-paper">
                    <img src={logo} alt=""/>
                </a>
                <ul className={`nav-menu-refinery-paper ${mobileShow ? "go" : ""}`}>
                    <li className="nav-item-refinery-paper cursor">
                        <span href="#" className="nav-link-refinery-paper t-white t-16-27 t-600">FAQ</span>
                    </li>
                </ul>
                <div className={`hamburger cursor ${mobileShow ? "go" : ""}`} onClick={() => handleMobileShow()} >
                    <span className="bar"></span>
                    <span className="bar"></span>
                    <span className="bar"></span>
                </div>
            </nav>
            </div>
        </header>
        <div className="section-hero-refinery-paper d-flex jcc aic">
            <span className="heading-hero-refinery-paper t-600 t-white">Welcome to our Refinery</span>
        </div>
        <div className="hero-bg"></div>
        <div className="section-tree-refinery-paper">
            <div className="heading-section-tree-refinery-paper d-flex jcc aic fdc">
                <span>What is Refinery?</span>
                <span>The refinery is our incubator.  Projects are sent through the refiner's fire and forged with passion and integrity into truly unique gems worthy of community trust.</span>
            </div>
            <div class="wrapper-tree">
                <div class="One d-flex jcc aic fdc">
                    <span className="title-card-tree">Why?</span>
                    <span className="desc-card-tree">Whether it’s lack of funding, insufficient marketing or outright dishonesty, most projects fail.  The refinery was created with the single purpose of making sure that ALL aspects of your project are tended to, giving you the greatest chance of success.</span>
                </div>
                <div class="Two d-flex jcc aic fdc">
                    <span className="title-card-tree">How Can We Change That?</span>
                    <span className="desc-card-tree">Our refiners have access to a multitude of tools that can identify, assess, and if suitable, help forge your project into a worthy gem.</span>
                </div>
                <div class="Three d-flex jcc aic fdc">
                    <span className="title-card-tree">Our refiners</span>
                    <span className="desc-card-tree">
                        <ul className="ul-special">
                            <li>The Expansion Team</li>
                            <li>The Development Team</li>
                            <li>The Marketing Team</li>
                            <li>The Graphics Team</li>
                            <li>The Management Team</li>
                        </ul>
                    </span>
                </div>
                <div class="Four d-flex jcc aic fdc">
                    <span className="title-card-tree">Expansion</span>
                    <span className="desc-card-tree">This is your first port of call on this epic journey of discovery and creation. Their mission is to liaise with you and share your vision. If you pass this quest and your gem is deemed pure, then a discussion can be had about using our Launchpad</span>
                </div>
                <div class="Five d-flex jcc aic fdc">
                    <span className="title-card-tree">Development</span>
                    <span className="desc-card-tree">We have a team of truly talented and committed devs, ready to forge your gem into a reality.
                    <ul className="ul-development">
                        <li>Coding in multiple languages</li>
                        <li>Smart contracts</li>
                        <li>AI integration</li>
                        <li>Dapp development</li>
                    </ul>
                    </span>
                </div>
                <div class="Six d-flex jcc aic fdc">
                    <span className="title-card-tree">Graphics</span>
                    <span className="desc-card-tree">All gems need an identity.  Creating a strong, robust brand instantly recognizable by the masses is essential. Our graphics team has a vast wealth of knowledge and experience that will help your gem shine.
                    <ul className="ul-graphic">
                        <li>Whitepapers</li>
                        <li>Websites</li>
                        <li>Banners</li>
                        <li>Logos and branding</li>
                    </ul>
                    </span>
                </div>
                <div class="Seven d-flex jcc aic fdc">
                    <span className="title-card-tree">Strategy and Finance</span>
                    <span className="desc-card-tree">Having a good strategy and the ability to finance are both essential for the success of any budding gem.
                    <ul className="ul-strategy">
                        <li>Finance</li>
                        <li>Tokenomics</li>
                        <li>Planning</li>
                        <li>Monetization</li>
                    </ul>
                    </span>
                </div>
                <div class="Eight d-flex jcc aic fdc">
                    <span className="title-card-tree">Community Services</span>
                    <span className="desc-card-tree">The community will be your greatest treasure. With an array of experience through building for various projects, we will use time-tested strategies to engage and grow your community.
                    <ul className="ul-community">
                        <li>Provide admins and moderators</li>
                        <li>Community management</li>
                        <li>Engagement and growth strategies</li>
                    </ul>
                    </span>
                </div>
                <div class="nine d-flex jcc aic fdc">
                    <span className="title-card-tree">Marketing</span>
                    <span className="desc-card-tree">Once you have your pure gem, forged from the fires of the refinery, ready for the world to see it in all its glory, now it’s time to market it.  Tapping into the right channels is key when launching your gem.
                    <ul className="ul-marketing">
                        <li>Present and feature you on our exchange</li>
                        <li>Feature you on our socials</li>
                        <li>Open access to our network of contacts</li>
                        <li>Guide you with your vision and ensure you take the right path</li>
                        <li>Form alliances that will last</li>
                    </ul>
                    </span>
                </div>
                <div class="ten d-flex jcc aic fdc">
                    <span className="title-card-tree">launchpad</span>
                    <span className="desc-card-tree">If you passed our forging process and made it on your quest to produce a gem you can be proud of, our Launchpad awaits you.
                    <ul className="ul-launchpad">
                        <li>Hand-picked gems may launch on our platform</li>
                        <li>Become part of the exclusive degem community</li>
                        <li>Tiered Staking models</li>
                        <li>Community voting models</li>
                        <li>Exclusive marketing expertise</li>
                        <li>Whitelist and private sale capabilities</li>
                        <li>IDOs in BUSD</li>
                        <li>Bot removal functionality</li>
                        <li>Vesting</li>
                        <li>Multiple launches at same time</li>
                    </ul>
                    </span>
                </div>
            </div>
            <div className="footer-section-tree-refinery-paper d-flex jcc aic fdc">
                <span>Degem</span>
                <span>Taking your visions and making them a reality!</span>
            </div>
        </div>
        <Footer
            color="#F75D19"
        />
        </div>
        </>
    )
}
export default Refinery_paper;
import React from 'react';
import { NavLink } from 'react-router-dom';

//asset
import logo from '../assets/asset_docpage/ic-logo.png';
import arrowrg from '../assets/asset_docpage/btn_arrow-right.png'
//asset style

// import '../components/style/global.css';

//globalcomponent
import Navbar from '../components/comp/navbar.js';
import Footer from '../components/comp/footer.js'


const Docpage = () => {
    require ('../css/docpage.css')
    return (
        <>
           <Navbar img={logo} color="#0E93F5"/>
            <div>

            <div className="section docpage-landing-sec1">
                <div className="container">

                  {/* <!-- sec-hero --> */}
                  <div className="docpage-main-box flex-c h-center">
                      
                      <h1 className=" doc-display t-center">
                      Get access to our documents
                      </h1>

                      <div class="btn-wrap">
                      <NavLink to="/" class="t-cursor doc-t-18-27" exact>
                      <a>
                        Whitepaper 
                        <img class="arrow" src={arrowrg}/>
                      </a>
                      </NavLink>
                      <NavLink to="/" class="t-cursor doc-t-18-27 doc-btn-black" exact>
                      <a>
                        One Pager
                        <img class="arrow" src={arrowrg}/>
                      </a>
                      </NavLink>

                      </div>
                  </div>
                </div>
              </div>



            </div>
           <Footer color="#0E93F5"/>
        </>
    );
};

export default Docpage;

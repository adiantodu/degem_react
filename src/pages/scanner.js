import React from 'react';

// asset 
import '../components/style/global.css';
import logoscanner from '../assets/asset_scanner/logo scaner.png';
import search from '../assets/asset_scanner/btn_search-alt.svg'

const Scanner = () => {
    require('../css/scanner.css')

    return (
        <>
        {/* navbar  */}
        <div className='t-navbar'>
            <div className='container'>
                <img src={logoscanner}/>
            </div>
        </div>

        <div className="section scanner-landing-sec1">
            <div className="container">
            {/* form  */}
                


            <form className="form-wrap">
                
                    <input className='input' type="text" placeholder="Enter token address"/>
                
                
                <button type="submit" className="submit">
                    <img src={search}/>
                </button>
         
            </form>


            </div>
        </div>
            
        </>
    );
};




export default Scanner;

import React from 'react';

import logo from '../assets/asset_dApp/degem-left-logo.svg'
import logo_dapp from '../assets/asset_dApp/treasury-logo.svg'
import logout_ico from '../assets/asset_dApp/logout-ico.svg'

import trans_ico from "../assets/asset_dApp/transaction-ico.svg"
import liquid_ico from "../assets/asset_dApp/liquid-ico.svg"
import binance_ico from "../assets/asset_dApp/binance-logo.svg"
import dgm_ico from "../assets/asset_dApp/dgm-ico.svg"

import copy_ico from "../assets/asset_dApp/copy-ico.svg"



function Dapp() {
    require('../css/dApp.css')
    require('../css/global.css')
    return (
        <>
        <div className="navbar">
                <div className="container flex-r just-space v-center">
                    <a className="cursor">
                        <img src={logo}/>
                    </a>
                    <a className="logout-btn cursor t-500 t-16-24 t-center">
                        <span className="m-hide">Log Out</span>
                        <img className="m-show" src={logout_ico}/>
                    </a>
                </div>
            </div>

        <div className="section dapp-main-content">
                <div className="container flex-r">

                    <img className="dapp-m-show dapp-m-logo" src={logo_dapp}/>

                    <div className="dapp-left-block">
                        <div className="dapp-neon-box left-itm flex-c v-center">
                            <img src={trans_ico} />
                            <span className="t-18-27 block">
                                Max transaction amount
                            </span>
                            <div className="t-600 t-28-42">
                                10.000.00
                                <span className="t-400 t-16-24">
                                    DGM
                                </span>
                            </div>
                        </div>
                        <div className="dapp-neon-box left-itm flex-c v-center">
                            <img src={liquid_ico} />
                            <span className="t-18-27 block">
                                Total Liquidity Pool
                            </span>
                            <div className="t-600 t-28-42">
                                $478,433.221
                            </div>
                        </div>
                        <div className="dapp-neon-box left-itm flex-c v-center">
                            <img src={binance_ico} />
                            <span className="t-18-27 block">
                                Total BNB in Liquidity Pool
                            </span>
                            <div className="t-600 t-28-42">
                                832.35
                                <span className="t-400 t-16-24">
                                    BNB
                                </span>
                            </div>
                        </div>
                        <div className="dapp-neon-box left-itm flex-c v-center">
                            <img src={dgm_ico} />
                            <span className="t-18-27 block">
                                Current $DGM Price
                            </span>
                            <div className="t-600 t-28-42">
                                $0.00326
                            </div>
                        </div>


                    </div>

                    <div className="dapp-right-block flex-c">

                        <img className="dapp-m-hide" src={logo_dapp}/>

                        <div className="dapp-neon-box dapp-address-block flex-r">
                            <div className="flex-c">
                                <span className="t-16-24">Your Address</span>
                                <div className="flex-r v-center">
                                    <span className="t-600 t-16-24">
                                        3FZbgi29cpjq2GjdwV8eyHuJJnkLtktZc5
                                    </span>
                                    <img className="cursor" onClick={() => {navigator.clipboard.writeText('3FZbgi29cpjq2GjdwV8eyHuJJnkLtktZc5')}} src={copy_ico}/>
                                </div>
                            </div>
                            <button className="cursor btn-1 t-18-27">
                                Buy DGM
                            </button>
                        </div>


                        <div className="dapp-neon-box dapp-reward-block flex-r">
                            <img src={binance_ico}/>
                            <div className="flex-c h-left">
                                <span className="t-24-36">
                                    My Reward 
                                </span>
                                <div className="t-40-60 t-600">
                                    0.000004 <span className="t-16-24 t-400">BNB</span>
                                </div>
                                <button className="cursor btn-1 t-18-27">
                                    Claim Reward
                                </button>
                            </div>
                        </div>


                        <div className="dapp-neon-box flex-c dapp-reward-pool-block">
                            <span className="t-18-27">
                                Reward Pool
                            </span>
                            <span className="t-28-42 t-600">
                                6.07 BNB
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default Dapp;

import $ from 'jquery'
import React, { useState } from "react";
import { NavLink, Link } from "react-router-dom";

import Navbar from '../components/comp/navbar.js'
import Footer from '../components/comp/footer.js'

import logo from '../assets/asset_refinery/logo-refinery.png'
import ic_center from '../assets/asset_refinery/ic-refinery-glow.png'
import bg_refinery from '../assets/asset_refinery/bg-refinery.png'
import ic_glow from '../assets/asset_refinery/ic-glow.png'
import ic_code from '../assets/asset_refinery/ic-code.svg'
import ic_pentool from '../assets/asset_refinery/ic-pentool.svg'
import ic_community from '../assets/asset_refinery/ic-community.svg'
import ic_marketing from '../assets/asset_refinery/ic-marketing.svg'
import ic_brain from '../assets/asset_refinery/ic-brain.svg'
import ic_bag from '../assets/asset_refinery/ic-bag.svg'




function Refinery(){
    require('../css/refinery.css')
    
    const [mobileShow, setMobileShow] = useState(false)
    const handleMobileShow = () => {
        setMobileShow(prev => !prev)
    }
    return(
        <>
    <div className="refinery">
        <header>
            <div className="container-refinery">
            <nav className="navbar-refinery">
                <a href="#" className="nav-logo-refinery">
                    <img src={logo} alt=""/>
                </a>
                <ul className={`nav-menu-refinery ${mobileShow ? "comeup" : ""}`}>
                    <NavLink to="/" target="_blank" exact>
                        <li className="nav-item-refinery cursor">
                            <span href="#" className="nav-link-refinery t-white t-16-27 t-600">Home</span>
                        </li>
                    </NavLink>
                    <NavLink to="/refinery_paper" target="_blank" exact>
                        <li className="nav-item-refinery cursor nav-center">
                            <span href="#" className="nav-link-refinery t-white t-16-27 t-600">Refinery Paper</span>
                        </li>
                    </NavLink>
                    <li className="nav-item-refinery cursor">
                        <span href="#" className="nav-link-refinery t-white t-16-27 t-600">FAQ</span>
                    </li>
                </ul>
                <div className={`hamburger cursor ${mobileShow ? "comeup" : ""}`} onClick={() => handleMobileShow()} >
                    <span className="bar"></span>
                    <span className="bar"></span>
                    <span className="bar"></span>
                </div>
            </nav>
            </div>
        </header>
        <div className="section refinery-sec1">
            <div className="container-refinery">
                <div className="refinery-main-box flex-c h-center">
                    <div className="ic-center-wrapper d-flex jcc aic fdc">
                        <img src={ic_glow} alt="ic-center" />
                        <span className="heading-section-hero">Refinery</span>
                    </div>
                    <p className="t-18-27 t-center">
                    Much like its namesake, the refinery team takes your raw, unfinished project and runs it through a series of audits and other services, turning it into a gem worthy of our launchpad.  
                    </p>
                    <NavLink to="/refinery_paper" target="_blank" exact className="btn-hero-refinery btn-orange t-18-27">Refinery Paper</NavLink>
                </div>
            </div>
        </div>
        <div className="section-bg"></div>
        <div className="section-service">
            <span className="heading-section-service d-flex jcc aic">Services Offered</span>
            <div class="grid-container">
                <div class="grid-item">
                    <span>Development</span>
                    <span>Contract development, DApps</span>
                    <div className="circle-orange d-flex jcc aic">
                        <img src={ic_code}/>
                    </div>
                </div>
                <div class="grid-item">
                    <span>Community Management</span>
                    <span>Fulltime or part time staff</span>
                    <div className="circle-orange d-flex jcc aic">
                        <img src={ic_community}/>
                    </div>
                </div>
                <div class="grid-item">
                    <span>Marketing</span>
                    <span>Social media advertising, AMA’s</span>
                    <div className="circle-orange d-flex jcc aic">
                        <img src={ic_marketing}/>
                    </div>
                </div>
                <div class="grid-item">
                    <span>Graphics</span>
                    <span>Web design, logo design, shill banners, whitepaper, and one pager</span>
                    <div className="circle-orange d-flex jcc aic">
                        <img src={ic_pentool}/>
                    </div>
                </div>
                <div class="grid-item">
                    <span>Project Management</span>
                    <span>Full or part time staff for key project roles</span>
                    <div className="circle-orange d-flex jcc aic">
                        <img src={ic_bag}/>
                    </div>
                </div>
                <div class="grid-item">
                    <span>Leadership Training</span>
                    <span>Courses and seminars on leadership</span>
                    <div className="circle-orange d-flex jcc aic">
                        <img src={ic_brain}/>
                    </div>
                </div>
            </div>
            <div className="section-service-footer d-flex jcc aic fdc">
                <span>For more detailed information, please refer to our refinery one pager</span>
                <button className="btn-service btn-orange">Apply To The Refinery</button>
            </div>
            </div>
        <Footer
            color="rgba(247, 93, 25, 1)"
        />
    </div>
        
        </>
    )
}
export default Refinery;